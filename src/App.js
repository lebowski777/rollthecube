import React, { Component } from 'react';
import Cube from './components/Cube/Cube.js';
import RollButton from './components/RollButton/RollButton.js';
import RollHistory from './components/RollHistory/RollHistory.js';
import RollGame from './components/RollGame/RollGame.js';
import CurrentScore from './components/CurrentScore/CurrentScore.js';
import Dialog from './components/Dialog/Dialog.js';
import Helpers from './helpers.js';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cubeValue: Helpers.getRandomValue(),
      rollHistory: [],
      rollGame: [],
      currentScore: 0,
      cellTurned: 0,
      cellTurnLimit: 3,
      gameRollLimit: 9,
      averageScore: 10,
      isDialogOpen: false
    };

    this.rollCube = this.rollCube.bind(this);
    this.clearHistory = this.clearHistory.bind(this);
    this.resetGame = this.resetGame.bind(this);
    this.addToCurrentScore = this.addToCurrentScore.bind(this);
    this.isGameOver = this.isGameOver.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
  }

  rollCube() {
    const newRollValue = Helpers.getRandomValue(1, 6, this.state.cubeValue);
    this.addToHistory(newRollValue);
    this.addToGame(newRollValue);
    this.setState(() => {
      return { cubeValue: newRollValue }
    });
  }

  clearHistory() {
    this.setState({
      rollHistory: []
    });
  }

  resetGame() {
    this.setState({
      rollGame: [],
      currentScore: 0,
      cellTurned: 0,
      isDialogOpen: false
    });
  }

  addToGame(rollValue) {
    let newGame = [...this.state.rollGame];
    const isVisible = this.state.rollGame.length < (this.state.gameRollLimit-1);
    newGame.push( {
      value: rollValue,
      isVisible: isVisible
    });
    newGame = newGame.map(item => ({
      ...item,
      isVisible: isVisible
    }));

    this.setState({
      rollGame: !isVisible ? Helpers.shuffle(newGame) : newGame
    });
  }

  addToHistory(rollValue) {
    let newHistory = [...this.state.rollHistory];
    newHistory.push(rollValue);
    this.setState({
       rollHistory: newHistory
    });
  }

  isGameOver() {
    return this.state.cellTurned === this.state.cellTurnLimit;
  }

  isFinishedRolling() {
    return this.state.rollGame.length >= this.state.gameRollLimit;
  }

  makeValueVisible(index) {
    let newGame = this.state.rollGame;
    newGame[index].isVisible = true;
    this.setState({
      rollGame: newGame
    });
  }

  addToCurrentScore(item, index) {
    this.makeValueVisible(index);
    this.setState((state) => {
      return {
        currentScore: state.currentScore + item.value,
        cellTurned: ++state.cellTurned,
        isDialogOpen: (this.state.cellTurned+1) === this.state.cellTurnLimit
      }
    });
  }

  closeDialog() {
    this.setState({
        isDialogOpen: false
    });
  }

  render() {
    return (
      <div className="app">
        <Dialog isOpen={this.state.isDialogOpen} closeDialog={this.closeDialog} averageScore={this.state.averageScore} finalScore={this.state.currentScore} resetGame={this.resetGame} />
        <header>
          <h1>Roll the Cube!</h1>
        </header>
        <div className="wrapper">
          {!this.isFinishedRolling() &&
          <div>
            <Cube cubeValue={this.state.cubeValue} />
            <RollButton rollCube={this.rollCube}/>
          </div>
          }
          <div>
            {this.isFinishedRolling() &&
            <CurrentScore currentScore={this.state.currentScore} />
            }
            <RollGame isGameOver={this.isGameOver} addToCurrentScore={this.addToCurrentScore} rollGame={this.state.rollGame} resetGame={this.resetGame} />
            {!this.isFinishedRolling() &&
            <RollHistory rollHistory={this.state.rollHistory} clearHistory={this.clearHistory}/>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default App;
