class Helpers {

  static getRandomValue(min = 1, max = 6, except = 0) {
    let num = Math.floor(Math.random() * (max - min + 1)) + min;
    return (num === except) ? this.getRandomValue(min, max, except) : num;
  }

  static shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }
}

export default Helpers;
