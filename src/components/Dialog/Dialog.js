import React, { Component } from 'react';
import './Dialog.css';

class Dialog extends Component {
  constructor(props) {
    super(props);
    this.handleResetClick = this.handleResetClick.bind(this);
    this.handleCloseClick = this.handleCloseClick.bind(this);
  }

  handleResetClick() {
    this.props.resetGame();
  }

  handleCloseClick() {
    this.props.closeDialog();
  }

  render() {
    if(this.props.isOpen) {
      return (
        <div className="dialog">
          <div className="dialog-wrapper">
            <div className={`dialog-content ${this.props.finalScore > this.props.averageScore ? 'successful': 'unsuccessful'}`}>
              <span className="close" onClick={this.handleCloseClick}>&times;</span>
              <div>
                <i className={`fas fa-${this.props.finalScore > this.props.averageScore ? 'smile': 'sad-tear'}`}></i>
                {this.props.finalScore > this.props.averageScore ? 'Congratulations! You did better then average!': 'Too bad! You did worst then average.'}
                <h2>Score: {this.props.finalScore}</h2>
              </div>
            </div>
            <div className="dialog-nav">
              <button onClick={this.handleResetClick}><i className="fas fa-redo"></i>Play again</button>
            </div >
          </div>
        </div>
      );
    }
    return null;
  }
}

export default Dialog;
