import React, { Component } from 'react';
import './RollGame.css';

class RollGame extends Component {
  constructor(props) {
    super(props);
    this.handleResetClick = this.handleResetClick.bind(this);
    this.handleCellClick = this.handleCellClick.bind(this);
  }

  handleResetClick() {
    this.props.resetGame();
  }

  handleCellClick(item, index) {
    this.props.addToCurrentScore(item, index);
  }

  render() {
    return (
      <div className={`rollGame gameOver-${this.props.isGameOver()}`}>
        <h3 className="rollGame__label">
          Roll game
          {this.props.rollGame.length > 0 &&
          <a className="rollGame__clear" onClick={this.handleResetClick}><i className="fas fa-redo"></i>Reset game</a>
          }
        </h3>
        {this.props.rollGame && this.props.rollGame.map((item, index) => (
          <div className={`rollGameItem visible-${item.isVisible}`} onClick={this.handleCellClick.bind(this, item, index)} key={index}>{(item.isVisible ? item.value : '?' )}</div>
        ))
        }

        {this.props.rollGame.length < 1 &&
        <div className="rollGame__notice">Start by clicking on "Roll baby roll!" button.</div>
        }
      </div>
    )
  }
}

export default RollGame;