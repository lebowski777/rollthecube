import React, { Component } from 'react';
import './CurrentScore.css';

class CurrentScore extends Component {
  render() {
    return (
      <div className="currentScore">
        Score: {this.props.currentScore}
      </div>
    );
  }
}

export default CurrentScore;
