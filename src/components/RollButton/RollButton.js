import React, { Component } from 'react';
import './RollButton.css';

class RollButton extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.rollCube();
  }

  render() {
    return (
      <button className="rollButton" onClick={this.handleClick}>
        Roll baby roll!
      </button>
    );
  }
}

export default RollButton;
