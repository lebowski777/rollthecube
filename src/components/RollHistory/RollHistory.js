import React, { Component } from 'react';
import './RollHistory.css';

class RollHistory extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.clearHistory();
  }

  render() {
    return (
      <div className="rollHistory">
        <h3 className="rollHistory__label">
          Roll history
          {this.props.rollHistory.length > 0 &&
          <a className="rollHistory__clear" onClick={this.handleClick}><i className="fas fa-times"></i>Clear History</a>
          }
        </h3>
        {this.props.rollHistory &&
          this.props.rollHistory.map(function (value, index) {
            return <div className="rollHistoryItem" key={index}>{value}</div>;
          })
        }

        {this.props.rollHistory.length < 1 &&
          <div className="rollHistory__notice">Roll history is empty.</div>
        }
      </div>
    )
  }
}

export default RollHistory;