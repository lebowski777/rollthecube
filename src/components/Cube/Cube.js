import React, { Component } from 'react';
import './Cube.css';

class Cube extends Component {

  render() {
    return (
      <div className="scene">
        <div className={`cube show-${this.props.cubeValue}`}>
          <div className="cube__face cube__face--1"><span>1</span></div>
          <div className="cube__face cube__face--2"><span>2</span></div>
          <div className="cube__face cube__face--3"><span>3</span></div>
          <div className="cube__face cube__face--4"><span>4</span></div>
          <div className="cube__face cube__face--5"><span>5</span></div>
          <div className="cube__face cube__face--6"><span>6</span></div>
        </div>
      </div>
    );
  }
}

export default Cube;